from openerp import models, fields, api
import openerp.addons.decimal_precision as dp

class sales_refund(models.Model):
    _name = 'sales.refund'

#     def action_cancel(self, cr, uid, ids, context=None):
#         if context is None:
#             context = {}
#             
#         return True
    
    def action_button_confirm(self, cr, uid, ids, context=None):

        obj_landing_cost=self.pool.get('stock.picking')
        data=self.browse(cr, uid, ids, context)
        
        obj_picking_type=self.pool.get('stock.picking.type')
        data_line = []
        
        if data:
            picking_type_id=obj_picking_type.search(cr, uid, [('name','=','Receipts'),('warehouse_id','=',data[0].warehouse_id.id)])
            
            
            
            stock_location_obj=self.pool.get('stock.location')
            locatin_ids=stock_location_obj.search(cr,uid,[('company_id','=',data[0].warehouse_id.id),('usage','=','view')])
            locatin_ids1=stock_location_obj.search(cr,uid,[('company_id','=',data[0].warehouse_id.id),('location_id','=',locatin_ids[0])])
            print locatin_ids1
            
            if data.refund_line_ids:
                for line in data.refund_line_ids:
                    data_line.append((0, 0, {
                                     'product_id':line.product_id.id,
                                     'name': line.name,
                                     'product_uom_qty':line.product_uom_qty,
                                     'product_uom':line.product_id.uom_id.id,
                                     'location_id':data[0].partner_id.property_stock_customer.id,
                                     'location_dest_id':locatin_ids1[0],
                                     'state':'confirmed'
                                          }))
                
                move_id = obj_landing_cost.create(cr, uid, {
                        'partner_id':data[0].partner_id.id,
                        'date': data[0].date_refund,
                        'min_date': data[0].date_refund,
                        'origin': data[0].name,
                        'state': 'confirmed',
                        'picking_type_id':picking_type_id[0],
                        'move_lines':data_line
                })            
                print "::::::::::::::::::", move_id
                self.write(cr, uid, ids, {'state': 'manual','picking_id' : move_id})
        
        return True
    
    def action_view_delivery(self, cr, uid, ids, context=None):
        '''
        This function returns an action that display existing delivery orders
        of given sales order ids. It can either be a in a list or in a form
        view, if there is only one delivery order to show.
        '''
        
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree_all')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]

        #compute the number of delivery orders to display
        pick_ids = []
        for so in self.browse(cr, uid, ids, context=context):
            pick_ids += [picking.id for picking in so.picking_id]
            
        #choose the view_mode accordingly
        if len(pick_ids) > 1:
            result['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = pick_ids and pick_ids[0] or False
        return result
    
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
             
        obj_stock_warehouse=self.pool.get('stock.warehouse')
        warehouse_id=obj_stock_warehouse.search(cr,uid,[])
        print "ssssssss",warehouse_id
             
         
        if vals.get('name', '/') == '/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'sales.refund') or '/'
#         if vals.get('partner_id') and any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id', 'fiscal_position']):
#             defaults = self.onchange_partner_id(cr, uid, [], vals['partner_id'], context=context)['value']
#             if not vals.get('fiscal_position') and vals.get('partner_shipping_id'):
#                 delivery_onchange = self.onchange_delivery_id(cr, uid, [], vals.get('company_id'), None, vals['partner_id'], vals.get('partner_shipping_id'), context=context)
#                 defaults.update(delivery_onchange['value'])
#             vals = dict(defaults, **vals)
        ctx = dict(context or {}, mail_create_nolog=True)
        new_id = super(sales_refund, self).create(cr, uid, vals, context=ctx)
#         self.message_post(cr, uid, [new_id], body=_("Quotation created"), context=ctx)
         
        return new_id
    
    def create_invoice(self, cursor, user, ids, context={}):
        time_order = self.browse(cursor, user, ids, context)
        partner_id = self.pool.get('res.partner').search(cursor, user, [('name', '=', time_order.partner_id.name)], context=context)
        print "name", partner_id    
        part_id = self.pool.get('res.partner').browse(cursor, user, partner_id, context=context) 
        print "name", part_id.name    
        
        account_journal_obj=self.pool.get('account.journal')
        journal_ids=account_journal_obj.search(cursor,user,[('type','=','sale_refund')])
        print "ddddddddddd",journal_ids
                
        account_invoice_obj = self.pool.get('account.invoice')
        account_invoice_line_obj = self.pool.get('account.invoice.line')
        for order_id in time_order:
            inv = {
                'account_id': part_id.property_account_receivable.id,
                'partner_id': part_id.id,
                'date_invoice': fields.date.today(),
                'journal_id':journal_ids[0],
                'type':'out_refund',
            }
            inv_id = account_invoice_obj.create(cursor, user, inv, context=None)
            
        for line in time_order.refund_line_ids:
            inv_line = {
                'product_id': line.product_id.id,
                'name':  line.product_id.name,
                'price_unit':line.product_id.lst_price,
                'invoice_id': inv_id,
                'quantity':line.product_uom_qty,
                'account_id':line.product_id.categ_id.property_account_income_categ.id,
                }
            account_invoice_line_obj.create(cursor, user, inv_line, context=None)
            
        self.write(cursor, user, order_id.id, {'invoice_id':inv_id, 'state':'done','seq_name': self.pool.get('ir.sequence').get(cursor, user, 'sales.refund') or '/'})    
        return True        
        
    def action_view_invoice(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        result = mod_obj.get_object_reference(cr, uid, 'account', 'action_invoice_tree1')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        #compute the number of invoices to display
        inv_ids = []
        for so in self.browse(cr, uid, ids, context=context):
            inv_ids += [so.invoice_id.id]
        #choose the view_mode accordingly
        if len(inv_ids)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, inv_ids))+"])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = inv_ids and inv_ids[0] or False
        return result
    
    
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        if not part:
            return {'value': {'partner_invoice_id': False, 'partner_shipping_id': False,  'payment_term': False, 'fiscal_position': False}}

        part = self.pool.get('res.partner').browse(cr, uid, part, context=context)
        addr = self.pool.get('res.partner').address_get(cr, uid, [part.id], ['delivery', 'invoice', 'contact'])
        pricelist = part.property_product_pricelist and part.property_product_pricelist.id or False
        payment_term = part.property_payment_term and part.property_payment_term.id or False
        dedicated_salesman = part.user_id and part.user_id.id or uid
        val = {
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'payment_term': payment_term,
            'user_id': dedicated_salesman,
        }
#         delivery_onchange = self.onchange_delivery_id(cr, uid, ids, False, part.id, addr['delivery'], False,  context=context)
#         val.update(delivery_onchange['value'])
        if pricelist:
            val['pricelist_id'] = pricelist
#         sale_note = self.get_salenote(cr, uid, ids, part.id, context=context)
#         if sale_note: val.update({'note': sale_note})  
        return {'value': val}

    def amount_total_function(self):
        res=self._amount_all_wrapper()
        key=res.keys()
        r=res[key[0]]
        
        for prd in self:
            prd.amount_untaxed = r['amount_untaxed']
            prd.amount_tax = r['amount_tax']
            prd.amount_total = r['amount_total']
            
        return True
        
    def _amount_line_tax(self, cr, uid, line, context=None):
        val = 0.0
        for c in self.pool.get('account.tax').compute_all(cr, uid, line.tax_id, line.price_unit, line.product_uom_qty, line.product_id, line.refund_id.partner_id)['taxes']:
            val += c.get('amount', 0.0)
        return val

    def _amount_all_wrapper(self, cr, uid, ids, context=None):
        """ Wrapper because of direct method passing as parameter for function fields """
        return self._amount_all(cr, uid, ids, context=context)

    def _amount_all(self, cr, uid, ids, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.refund_line_ids:
                val1 += line.price_subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
                
            print val,val1, cur_obj.round(cr, uid, cur, val1)
            res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
            res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
            res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
        return res
    
    @api.model
    def _get_default_warehouse(self):
#         res = self.pool.get('stock.warehouse').search(cr, uid, [], context=context)
        res=self.env['stock.warehouse'].search([])
        return res and res[0] or False
        return res
    
    
    
    name =fields.Char('Order Reference')
    client_order_ref = fields.Char('Reference/Description', copy=False)
    state = fields.Selection([
            ('draft', 'Draft Refund'),
            ('confirm', 'Confirm'),
            ('manual', 'Sale to Invoice'),
            ('done', 'Done') ], 'Status', default='draft', readonly=True, copy=False)
    date_refund = fields.Datetime('Date', required=True, default=fields.Date.today())
    warehouse_id=fields.Many2one('stock.warehouse', 'Warehouse',  default=lambda self: self._get_default_warehouse(), required=True)
    partner_id = fields.Many2one('res.partner', 'Customer')
    reference = fields.Char('Reference/Description')
    amount_tax = fields.Float(compute='amount_total_function', string='Taxes')
    amount_untaxed = fields.Float(compute='amount_total_function' ,string='Untaxed Amount')
    amount_total = fields.Float(compute='amount_total_function' ,string='Total')
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, help="Pricelist for current sales order.")
    refund_line_ids = fields.One2many('sales.refund.line', 'refund_id', 'Refund Line')
    incoterm = fields.Many2one('stock.incoterms', 'Incoterm')
    note = fields.Text('Terms and conditions')
    
    incoterm = fields.Many2one('stock.incoterms', 'Incoterm')
    picking_policy = fields.Selection([('direct', 'Deliver each product when available'), ('one', 'Deliver all products at once')], default='direct', required=True)
    order_policy  = fields.Selection([('manual', 'On Demand'),('picking', 'On Delivery Order'),('prepaid', 'Before Delivery')], 'Create Invoice', default='manual', required=True)
    payment_term = fields.Many2one('account.payment.term', 'Payment Term')
    fiscal_position = fields.Many2one('account.fiscal.position', 'Fiscal Position')
    company_id = fields.Many2one('res.company', 'Company')
    user_id = fields.Many2one('res.users', 'Salesperson')
    origin = fields.Char('Source Document')
    invoiced = fields.Boolean('Paid')
    shipped = fields.Boolean('Delivered')
    invoice_id = fields.Many2one('account.invoice', 'Invoice')    
    picking_id = fields.Many2one('stock.picking', 'Picking')        

class sales_refund_line(models.Model):
    _name = "sales.refund.line"
     
    order_id = fields.Many2one('sales.order', 'Order Reference', required=True, ondelete='cascade', select=True, readonly=True, states={'draft':[('readonly',False)]}),
    refund_id = fields.Many2one('sales.refund', 'Refund Id')
    product_id = fields.Many2one('product.product', 'Product')
    name = fields.Char('Description', requiured=True)
    product_uom_qty = fields.Float('Quantity', default=1.0, requiured=True)
    price_unit = fields.Float('Unit Price', requiured=True)
    tax_id = fields.Many2many('account.tax', 'sale_order_refund_tax', 'sales_redund_order_line_id', 'sales_redund_tax_id', 'Taxes')
    price_subtotal = fields.Float(compute='_amount_line', store=True, string='Subtotal')
    
    def onchange_product_id(self, cr, uid, ids, product_id, context=None):
        res = {}
        if product_id:
            obj = self.pool.get('product.product').browse(cr, uid, product_id)
            res['name'] = obj.name
            res['price_unit'] = obj.lst_price
        return {'value': res}
    
    @api.depends('price_unit','product_uom_qty')
    def _amount_line(self):
        
        for record in self:
            record.price_subtotal = record.price_unit * record.product_uom_qty


    
    