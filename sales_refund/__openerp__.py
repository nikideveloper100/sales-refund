{
    'name' : 'Sales Refund Module',
    'version' : '1.0',
    'author' : 'Niks',
    'category' : 'Sales Refund',
    'description' : """
    
    """,
    'website': 'nothing',
    'images' : [],
    'depends' : ['base', 'account', 'account_budget', 'sale'],
    'data': [
        "sales_refund_view.xml",
        "sequence_view.xml"
    ],
    'installable': True,
    'auto_install': False,
}